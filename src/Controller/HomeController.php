<?php

namespace App\Controller;

class HomeController extends AppController
{
    public function indexAction()
    {
        $this->set('title', 'cakephp test project');
    }
}