<?php

namespace App\Controller\Api;

use Cake\ORM\TableRegistry;
use App\Model\Entity\Api\QELogRequest;
use App\Model\Entity\Api\QELogResponse;
use Cake\Datasource\Exception\RecordNotFoundException;

class QEController extends BaseController
{
    const STATUS_ERROR         = -1;
    const STATUS_NO_SOLUTION   = 0;
    const STATUS_ONE_SOLUTION  = 1;
    const STATUS_TWO_SOLUTIONS = 2;

    /**
     * the same response over this controller
     *
     * @param integer $status
     * @param string  $message
     * @param float   $x1
     * @param float   $x2
     */
    protected function customResponse($status, string $message, float $x1 = null, float $x2 = null)
    {
        $this->jsonResponse([
            'status'  => $status,
            'message' => $message,
            'x1'      => $x1,
            'x2'      => $x2,
        ]);
    }

    /**
     * ACTION
     * solve quadratic equation
     *
     * @param string $a
     * @param string $b
     * @param string $c
     * @param string $token
     */
    public function solveAction()
    {
        $a     = $this->request->getData('a');
        $b     = $this->request->getData('b');
        $c     = $this->request->getData('c');
        $token = $this->request->getData('token');

        $errors = $this->validateParameters($a, $b, $c, $token);

        if (!empty($errors)) {
            return $this->customResponse(self::STATUS_ERROR, json_encode($errors));
        }

        // valid - create request log or increment counter
        $this->createOrUpdateRequestLog($token, $a, $b, $c);

        // all input parameters are valid - let's check if we have response
        $logResponse = $this->findLogResponseByToken($token);

        if (null === $logResponse) {
            // solve, create new response and save to db
            $logResponse = $this->generateNewResponse($token, $a, $b, $c);
        }

        $this->customResponse(
            $logResponse->get('status'),
            $logResponse->get('message'),
            $logResponse->get('x1'),
            $logResponse->get('x2')
        );
    }

    /**
     * @param string $a
     * @param string $b
     * @param string $c
     * @param string $token
     *
     * @return array
     */
    private function validateParameters(string $a, string $b, string $c, string $token)
    {
        $errors = [];
        if (!is_numeric($a) || (((integer) $a) != $a)) {
            $errors['a'] = 'a not number?';
        } elseif ($a <= 0) {
            $errors['a'] = 'a <=0?';
        }
        if (!is_numeric($b)) {
            $errors['b'] = 'b not number?';
        }
        if (!is_numeric($c)) {
            $errors['c'] = 'c not number?';
        }

        $hash = sha1($a . '|' . $b . '|' . $c);
        if ($hash !== $token) {
            $errors['main'] = 'invalid token';
        }

        return $errors;
    }

    /**
     * @param string $token
     * @param string $a
     * @param string $b
     * @param string $c
     */
    private function createOrUpdateRequestLog(string $token, string $a, string $b, string $c)
    {
        $logRequestsTable = TableRegistry::getTableLocator()->get('Api.QELogRequests');
        $logRequestsTable->setEntityClass(QELogRequest::class);

        try {
            $logRequest = $logRequestsTable->get($token);
            $logRequest->set('counter', $logRequest->get('counter') + 1);
        } catch (RecordNotFoundException $ex) {
            $counter = 1;
            $logRequest = $logRequestsTable->newEntity(compact('token', 'a', 'b', 'c', 'counter'));
        }
        $logRequestsTable->save($logRequest);
    }

    /**
     * @param string $token
     *
     * @return QELogResponse|null
     */
    private function findLogResponseByToken(string $token)
    {
        $logResponsesTable = TableRegistry::getTableLocator()->get('Api.QELogResponses');
        $logResponsesTable->setEntityClass(QELogRequest::class);

        try {
            return $logResponsesTable->get($token);
        } catch (RecordNotFoundException $ex) {
            return null;
        }
    }

    /**
     * @param string $token
     * @param string $a
     * @param string $b
     * @param string $c
     *
     * @return QELogResponse
     */
    private function generateNewResponse(string $token, string $a, string $b, string $c)
    {
        list ($status, $message, $x1, $x2) = $this->solveQuadraticEquation($a, $b, $c);

        $logResponsesTable = TableRegistry::getTableLocator()->get('Api.QELogResponses');
        $logResponse       = $logResponsesTable->newEntity(compact('token', 'status', 'message', 'x1', 'x2'));

        $logResponsesTable->save($logResponse);

        return $logResponse;
    }

    /**
     * @param string $a
     * @param string $b
     * @param string $c
     *
     * @return array
     */
    private function solveQuadraticEquation(string $a, string $b, string $c)
    {
        $d = $b * $b - 4 * $a * $c;
        if ($d < 0) {
            return [ self::STATUS_NO_SOLUTION, 'no solution', null, null, ];
        } elseif ($d == 0) {
            $x = ($b * -1) / (2 * $a);

            return [ self::STATUS_ONE_SOLUTION, 'one solution', $x, null, ];
        }

        $squareRootD = sqrt($d);
        $x1 = ((-1 * $b) - $squareRootD) / (2 * $a);
        $x2 = ((-1 * $b) + $squareRootD) / (2 * $a);

        if ($x1 === $x2) {
            return [ self::STATUS_ONE_SOLUTION, 'one solution', $x1, null, ];
        }

        return [ self::STATUS_TWO_SOLUTIONS, 'two solutions', $x1, $x2, ];
    }
}