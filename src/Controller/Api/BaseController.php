<?php

namespace App\Controller\Api;

use App\Controller\AppController;

class BaseController extends AppController
{
    protected function jsonResponse(array $data, $responseCode = 200)
    {
        $this->loadComponent('RequestHandler');
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->withType('application/json');
        $this->set([
            'result' => $data,
            '_serialize' => 'result',
        ]);
        $this->response->withStatus($responseCode);
    }
}