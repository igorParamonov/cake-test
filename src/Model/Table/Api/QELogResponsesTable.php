<?php

namespace App\Model\Table\Api;

use Cake\ORM\Table;

/**
 * CREATE TABLE q_e_log_responses (
 *      `token` VARCHAR(40) NOT NULL,
 *      `status` INT NOT NULL,
 *      `message` VARCHAR(255) NOT NULL,
 *      `x1` FLOAT NULL,
 *      `x2` FLOAT NULL,
 *      PRIMARY KEY `token` (`token`)
 * )
 */
class QELogEntriesTable extends Table
{
}