<?php

namespace App\Model\Table\Api;

use Cake\ORM\Table;

/**
 * CREATE TABLE q_e_log_requests (
 *      `token` VARCHAR(40) NOT NULL,
 *      `a` INT NOT NULL,
 *      `b` INT NOT NULL,
 *      `c` INT NOT NULL,
 *      `counter` INT NOT NULL DEFAULT 0,
 *      PRIMARY KEY `token` (`token`)
 * )
 */
class QELogEntriesTable extends Table
{
}