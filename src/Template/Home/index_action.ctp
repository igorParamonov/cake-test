<div class="row">
    <div class="col s12">
        <h2 class="orange-text text-darken-4">Quadratic Equation Solver</h2>
        <form id="solve-quadratic-equation-form" class="card-panel white" action="/api/qe-solve">
            <input placeholder="a" type="text" required name="a" autocomplete="off" /><span class="helper-text" data-error=""></span> * x<sup>2</sup> +
            <input placeholder="b" type="text" required name="b" autocomplete="off" /><span class="helper-text" data-error=""></span> * x +
            <input placeholder="c" type="text" required name="c" autocomplete="off" /><span class="helper-text" data-error=""></span> = 0
            <input type="submit" class="btn orange darken-4 right white-text" value="calculate" />
        </form>
    </div>
</div>

<div id="calculation-results-modal" class="modal">
    <div class="modal-content">
        <h4>calculation results</h4>
        <p id="results-container"></p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close orange darken-4 btn-flat white-text">ok</a>
    </div>
</div>